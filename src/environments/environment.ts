// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyDS3XuF-Uq6JmH4mKvJJVh_GvBTP7e1QPs",
    authDomain: "train-e5161.firebaseapp.com",
    databaseURL: "https://train-e5161.firebaseio.com",
    projectId: "train-e5161",
    storageBucket: "train-e5161.appspot.com",
    messagingSenderId: "8531461137",
    appId: "1:8531461137:web:ff5c6e066ecce6120c1db0"

  }
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
