import { PostsService } from './../posts.service';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-addpost',
  templateUrl: './addpost.component.html',
  styleUrls: ['./addpost.component.css']
})
export class AddpostComponent implements OnInit {
  title:string;
  author:string;
  body:string;
  Email:string;
  id:string;
  isEdit:boolean = false;
  buttonText:string = "Add post"
  constructor(private postsservice:PostsService,private router:Router,private route:ActivatedRoute) { }

  ngOnInit() {
    this.id = this.route.snapshot.params.id;
        if(this.id) {
          this.isEdit = true;
          this.buttonText = "update Post";
          this.postsservice.getPost(this.id).subscribe(
            Post => {
              this.author = Post.data().author;
              this.title = Post.data().title;
              this.body=Post.data().body;
              this.Email=Post.data().email;
            }
          )
        }
  }

  onSubmit(){ 
    if(this.isEdit){

      this.postsservice.editPost(this.id, this.title, this.author, this.body, this.Email)
    } 
    else{
      this.postsservice.addPost(this.title, this.author, this.body, this.Email)
    }
   
    this.router.navigate(['/posts']);
  }  


  
  }  


