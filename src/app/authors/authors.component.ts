import { BooksComponent } from './../books/books.component';
import { Component, OnInit } from '@angular/core';
import { BooksService } from '../books.service';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-authors',
  templateUrl: './authors.component.html',
  styleUrls: ['./authors.component.css']
})
export class AuthorsComponent implements OnInit {
  //books:object[] = [{title:'Alice in Wonderland', author:'Lewis Carrol'},{title:'War and Peace', author:'Leo Tolstoy'}, {title:'The Magic Mountain', author:'Thomas Mann'}, {title:'Michal', author:'Amos Os'}]
  books$:Observable<any>;
  constructor(public booksservice:BooksService
    ) { }
  ngOnInit() {
  //  this. books$ = this.booksservice.getBooks();
  }

}

