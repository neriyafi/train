import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { NavComponent } from './nav/nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { BooksComponent } from './books/books.component';
import {MatExpansionModule} from '@angular/material/expansion';
import { RouterModule, Routes } from '@angular/router';
import { AuthorsComponent } from './authors/authors.component';
import { EditauthorsComponent } from './editauthors/editauthors.component';
import { FormsModule }   from '@angular/forms';
import {MatCardModule} from '@angular/material/card';
import { PostsComponent } from './posts/posts.component';

import { HttpClientModule } from '@angular/common/http';
import { AddbookComponent } from './addbook/addbook.component';

import { AngularFireModule } from '@angular/fire';
import { AngularFirestore } from '@angular/fire/firestore';
import { environment } from '../environments/environment';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AddpostComponent } from './addpost/addpost.component';
import { AngularFireAuth } from '@angular/fire/auth';
import { SignupComponent } from './signup/signup.component';
import { LoginComponent } from './login/login.component';
import { DocformComponent } from './docform/docform.component';
import { ClassifiedComponent } from './classified/classified.component';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatSelectModule} from '@angular/material';
import {MatInputModule} from '@angular/material';

const appRoutes: Routes = [
  { path: 'books', component: BooksComponent },
  { path: 'authors', component: AuthorsComponent },
  { path: 'addbook', component: AddbookComponent },
  { path: 'addbook/:id', component: AddbookComponent },
  { path: 'addpost', component: AddpostComponent },
  { path: 'addpost/:id', component: AddpostComponent },
  { path: 'posts', component: PostsComponent },
  { path: 'editauthors/:id', component: EditauthorsComponent },
  { path: 'signup', component: SignupComponent },
  { path: 'login', component: LoginComponent },
  { path: 'docform', component: DocformComponent },
  { path: 'classified', component: ClassifiedComponent },
  { path: "",
    redirectTo: '/books',
    pathMatch: 'full'
  },
];

@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    BooksComponent,
    AuthorsComponent,
    EditauthorsComponent,
    PostsComponent,
    AddbookComponent,
    AddpostComponent,
    SignupComponent,
    LoginComponent,
    DocformComponent,
    ClassifiedComponent,

    
  ],
  imports: [
    RouterModule,
   
    //AngularFirestore,
    AngularFireModule,
    BrowserModule,
    AngularFireModule.initializeApp(environment.firebase),
    HttpClientModule,
    MatCardModule,
    MatSelectModule,
    FormsModule,
    BrowserModule,
    NoopAnimationsModule,
    LayoutModule,
    MatInputModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatExpansionModule,
   //AngularFireAnalyticsModule, // dynamically imports firebase/analytics
    AngularFirestoreModule, // imports firebase/firestore, only needed for database features
    AngularFireAuthModule, // imports firebase/auth, only needed for auth features,
    AngularFireStorageModule,
    MatFormFieldModule,
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true } )
  ],
  bootstrap: [ AppComponent ],
  providers: [ AngularFirestore, AngularFireAuth ],
})
export class AppModule { }
