import { Component, OnInit } from '@angular/core';
import { Posts } from '../interfaces/posts';
import { PostsService } from '../posts.service';
import { Users } from '../interfaces/users';
import {catchError } from 'rxjs/operators';
import { HttpErrorResponse } from '@angular/common/http';
import { throwError, Observable } from 'rxjs';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {
  
  // postsdata$: Posts[]=[];
  postsdata$:Observable<any>;
  message:string;
  constructor(private postsservice:PostsService) { }
  
  ngOnInit() {
    // this.postsservice.getPosts().subscribe(data =>this.postsdata$ = data);
    this.postsdata$ = this.postsservice.getPosts();
   // this.postsservice.getUsers().subscribe(data =>this.usersdata$ = data);
   this.message = "welcome to posts";
  }
  deletePost(id:string){
    this.postsservice.deletePost(id)
    this.message = "post deleted"
  }

  
  // עבודה עם API
  // postsdata$: Posts[]=[];
  // usersdata$: Users[]=[];
  // author:string;
  // authorMail:string;
  // message:string;
  // constructor(private postsservice:PostsService) { }
  
  // private handleError(res:HttpErrorResponse){
  //   console.log(res.error);
  //   return throwError("res.error")  }

  // ngOnInit() {
  //   this.postsservice.getPosts().subscribe(data =>this.postsdata$ = data);
  //   this.postsservice.getUsers().subscribe(data =>this.usersdata$ = data);
  // }

  // savePosts() {
  //  for (let index = 0; index <this.postsdata$.length; index++) {
  //    for (let i = 0; i <this.usersdata$.length; i++) {
  //      if(this.postsdata$[index].userId == this.usersdata$[i].id){
  //         this.postsservice.addPost(this.postsdata$[index].title, this.usersdata$[i].name,this.postsdata$[index].body, this.usersdata$[i].email )
  //      }    
  //    }  
  //  }
  //  this.message = "all uploaded ";
  // }
  // deleteAllPost() {
  //   let postsdata$ = this.postsservice.getPosts();
  //     for (let index = 0; index <this.postsdata$.length; index++) {
  //            let id = this.postsdata$[index].id;
  //           this.postsservice.deletePost(id);
  //     }
  //     this.message = "all the posts are deleted";
  //    }
 
}
