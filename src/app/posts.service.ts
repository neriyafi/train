import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Posts } from './interfaces/posts';
import { Users } from './interfaces/users';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
 export class PostsService {

  constructor(private db:AngularFirestore) { }
  
  getPosts():Observable<any[]>{
    return this.db.collection('posts').valueChanges({idField:'id'});
  }
  getPost(id:string){
    return this.db.collection('posts').doc(id).get();
  }
  deletePost(id:string){
    this.db.collection('posts').doc(id).delete();
  }
  addPost(title:string, author:string, body:string, Email:string){
    const post = {title:title , author:author, body:body, email:Email}
    this.db.collection('posts').add(post);
  }

  editPost(id:string, title:string, author:string, body:string, Email:string){
    const post = {title:title , author:author, body:body, email:Email}
    this.db.collection('posts').doc(id).update(post);
  }
}

//עבודה מול API
// // private URL="https://jsonplaceholder.typicode.com/posts/?userId=";
// // userId = '1';
// //id = '1'
// private POSTURL="https://jsonplaceholder.typicode.com/posts"
// private USERURL="https://jsonplaceholder.typicode.com/users/"

// constructor(private http:HttpClient, private db:AngularFirestore) { }

//     getPosts():Observable<Posts[]>{
//    // return this.http.get<Posts[]>(`${this.URL}${this.userId}&id=${this.id}`);
//    return this.http.get<Posts[]>(`${this.POSTURL}`);
//    }

//    getUsers():Observable<Users[]>{
//     return this.http.get<Users[]>(`${this.USERURL}`);
//     }

//     addPost(title:string, author:string,body:string, email:string){
//       const post = {title:title, author:author,body:body , email:email}
//       this.db.collection('posts').add(post);
//     }
  
