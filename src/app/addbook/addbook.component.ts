import { Component, OnInit } from '@angular/core';
import { BooksService } from '../books.service';
import { ActivatedRoute,Router } from '@angular/router';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-addbook',
  templateUrl: './addbook.component.html',
  styleUrls: ['./addbook.component.css']
})
export class AddbookComponent implements OnInit {
  id:string;
  title:string; 
  author:string;
  book:any;
  isEdit:boolean = false;
  buttonText:string = "Add book";
  userId: string;

  constructor(public booksservice:BooksService,private route:ActivatedRoute,private router:Router, private authservice:AuthService) { }

  ngOnInit() {
    this.id = this.route.snapshot.params.id;
    this.authservice.user.subscribe(
      user=> {
        this.userId = user.uid;
        //relevant only for update book
        if(this.id){
          this.isEdit = true;
          this.buttonText = "Update Book";
          this.booksservice.getBook(this.id, this.userId).subscribe(
            book => {
              this.author = book.data().author;
              this.title = book.data().title;
            })
        }
      }
    )
      }


onSubmit(){ 
  if(this.isEdit){
    this.booksservice.updatebook(this.userId,this.id, this.title, this.author)
  } 
  else{
    this.booksservice.addbook(this.userId, this.title, this.author)
  }
 
  this.router.navigate(['/books']);
}  

}
