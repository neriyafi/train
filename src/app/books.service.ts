import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class BooksService {
  books:any ;   //= [{id:1,title:'Alice in Wonderlandeddd', author:'Lewis Carrol'},{id:2,title:'War and Peace', author:'Leo Tolstoy'}, {id:3,title:'The Magic Mountain', author:'Thomas Mann'}, {id:4,title:'Michal', author:'Amos Os'}];
  book:any;
  userCollection: AngularFirestoreCollection = this.db.collection('users');
  bookCollection: AngularFirestoreCollection;
  constructor(private db:AngularFirestore) { }
  
getBooks(userId:string):Observable<any[]>{
 // return this.db.collection('books').valueChanges({idField:'id'});
 this.bookCollection = this.db.collection(`users/${userId}/books`);
  return this.bookCollection.snapshotChanges().pipe(
    map(
      collection => collection.map(
        document => {
          const data = document.payload.doc.data();
          data.id = document.payload.doc.id;
          return data;
        }
      )

    )
  )

}

getBook(id:string, userId:string){
  //return this.db.collection('books').doc(id).get();
 return this.db.doc(`users/${userId}/books/${id}`).get()
}

addbook(userId:string, title:string, author:string){
  const book = {title:title, author:author}
  //this.db.collection('books').add(book);
  this.userCollection.doc(userId).collection('books').add(book);
}

deletebook(id:string , userId:string){
 // this.db.collection('books').doc(id).delete();
 this.db.doc(`users/${userId}/books/${id}`).delete();
}

updatebook(userId:string, id:string,title:string, author:string){
   //const book = {title:title, author:author}
  // this.db.collection('books').doc(id).update(book);
  this.db.doc(`users/${userId}/books/${id}`).update({
    title:title,
    author:author
  })

}

}


  /*  קריאה של הדטה מתוך גייסון בסרוויס

  getBooks(){
    const booksObservable = new Observable(
      observer => {
        setInterval(
          () => observer.next(this.books),500
        )
      }
    )
    return booksObservable;
  }
  /*
  getBook(id:string){
    for (let index = 0; index < this.books.length; index++) {
      console.log(id);
      if(this.books[index].id == id){
      return this.books[index].author;
      }
    }
  }

  editauth(id:string,author:string){
    for (let index = 0; index < this.books.length; index++) {
      console.log(id);
      if(this.books[index].id == id){
      this.books[index].author=author;
      }
    }
  }

  addbook(){
   // setInterval(
     // () => this.books.push({id:'5' , title:'title', author :'author'}),500
    //  ) 
     this.books.push({id:'5' , title:'title', author :'author'})
  }
*/
