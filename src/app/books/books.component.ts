import { Component, OnInit } from '@angular/core';
import {MatExpansionModule} from '@angular/material/expansion';
import { BooksService } from '../books.service';
import { Observable } from 'rxjs';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {
  books:any;
  books$:Observable<any>;
  userId:string;
  //title:string='cool';
  //books:object[] = [{title:'Alice in Wonderland', author:'Lewis Carrol'},{title:'War and Peace', author:'Leo Tolstoy'}, {title:'The Magic Mountain', author:'Thomas Mann'}, {title:'Michal', author:'Amos Os'}]
  constructor(public booksservice:BooksService , public authservice:AuthService) { }

  ngOnInit() {
   // this.books = this.bookservice.getBooks().subscribe(
    //(books) => this.books = books)
   //this.books$ = this.booksservice.getBooks(); 
   this.authservice.user.subscribe(
    user => {
      this.userId = user.uid;
      this.books$ = this.booksservice.getBooks(this.userId);
    }
  )

  }

  deletebook(id:string){
    this.booksservice.deletebook(id , this.userId);
  }

}
