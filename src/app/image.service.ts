import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ImageService {

  public images:string[] = [];
  public path:string = 'https://firebasestorage.googleapis.com/v0/b/train-e5161.appspot.com/o/'
 
 constructor() {
  this.images[0] = this.path +'biz (1).JPG' + '?alt=media';
  this.images[1] = this.path +'entermnt (1).JPG' + '?alt=media';
  this.images[2] = this.path +'politics-icon (1).png' + '?alt=media';
  this.images[3] = this.path +'תמונה1.png' + '?alt=media';
  this.images[4] = this.path +'tech (1).JPG' + '?alt=media';
 }

}
 